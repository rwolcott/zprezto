#
# Defines Bazel aliases.
#

#
# Settings
#
#

# This way the completion script does not have to parse Bazel's options
# repeatedly.  The directory in cache-path must be created manually.
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path /tmp/zsh/cache

#
# Aliases
#

function compile() {
  bazel --max_idle_secs=0 build --experimental_action_listener=//tools/actions:generate_compile_commands_listener $@
  bazel_success=$?

  if [ $bazel_success -eq 0 ]; then
    notify-send Success! $@
    #paplay /usr/share/sounds/freedesktop/stereo/complete.oga
    #paplay /usr/share/sounds/ubuntu/stereo/dialog-information.ogg
    paplay /usr/share/sounds/KDE-Sys-App-Positive.ogg
    (~/driving/src/tools/actions/generate_compile_commands_json.py > /dev/null 2>&1 &)
  else
    notify-send FAIL! $@
    #espeak f
    #paplay /usr/share/sounds/ubuntu/stereo/dialog-error.ogg
    #paplay /usr/share/sounds/ubuntu/stereo/dialog-warning.ogg
    paplay /usr/share/sounds/KDE-Sys-App-Error-Serious.ogg
  fi
}

#alias bazel='bazel --max_idle_secs=0 $@; alert hi'

alias bb='bazel build --explain=/tmp/bazel_explain_log --verbose_explanations -s //...'
